<?php

namespace SylvainLG\Application;

use \Pimple\Container;
use \MongoDB\Client as MongoClient;

class PimpleProvider implements \Pimple\ServiceProviderInterface {

	public function register(Container $container) {

		$container['config'] = function($c) {
			return new \SylvainLG\Application\AppConfig($c);
		};

		$container['router'] = function($c) {
			return new \Phroute\Phroute\RouteCollector();
		};

		$container['log'] = function($c) {
			// create a log channel
			$loglevel = $container['config']->get('loglevel') ?? 'error';

			$log = new Logger('default');
			$handler = new ErrorLogHandler(ErrorLogHandler::OPERATING_SYSTEM, Logger::toMonologLevel($loglevel));
			$handler->setFormatter(new \Bramus\Monolog\Formatter\ColoredLineFormatter());
			$log->pushHandler($handler);
			return $log;
		};

	}

}